package com.eswar.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/welcome")
public class WelcomeController {

	@RequestMapping(value = "/{str}", method=RequestMethod.GET)
	public String printController(@PathVariable String str, ModelMap modelMap){
		System.out.println(WelcomeController.class.getName());
		modelMap.addAttribute("name", str);
		return "index";
	}
	
}
